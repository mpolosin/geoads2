
drop table if exists `AuthAssignment`;
drop table if exists `AuthItemChild`;
drop table if exists `AuthItem`;

create table `AuthItem`
(
   `name`                 varchar(64) not null,
   `type`                 integer not null,
   `description`          text,
   `bizrule`              text,
   `data`                 text,
   primary key (`name`)
) engine InnoDB;

create table `AuthItemChild`
(
   `parent`               varchar(64) not null,
   `child`                varchar(64) not null,
   primary key (`parent`,`child`),
   foreign key (`parent`) references `AuthItem` (`name`) on delete cascade on update cascade,
   foreign key (`child`) references `AuthItem` (`name`) on delete cascade on update cascade
) engine InnoDB;

create table `AuthAssignment`
(
   `itemname`             varchar(64) not null,
   `userid`               varchar(64) not null,
   `bizrule`              text,
   `data`                 text,
   primary key (`itemname`,`userid`),
   foreign key (`itemname`) references `AuthItem` (`name`) on delete cascade on update cascade
) engine InnoDB;


DROP TABLE IF EXISTS `auth_users`;
CREATE TABLE IF NOT EXISTS `auth_users`(
	`id` int(11) not null AUTO_INCREMENT,
	`login` varchar(50) not null,
	`password` varchar(100) not null,
	`salt` varchar(100) default null,
	`date_create` TIMESTAMP default '0000-00-00 00:00:00',
        `date_updated` TIMESTAMP  default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	 PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8;

DROP TABLE IF EXISTS `profile_clients`;
CREATE TABLE IF NOT EXISTS `profile_clients`(
	`id` int(11) not null AUTO_INCREMENT,
	`first_name` varchar(50) not null,
	`second_name` varchar(50) not null,
	`middle_name` varchar(50) default null,
	`phone` varchar(50) default null,
	`email` varchar(50) default null,
	`auth_user_id` int(11) not null,
        `date_create` TIMESTAMP default '0000-00-00 00:00:00',
        `date_updated` TIMESTAMP  default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8;

DROP TABLE IF EXISTS `profile_companies`;
CREATE TABLE IF NOT EXISTS `profile_companies`(
	`id` int(11) not null AUTO_INCREMENT,
	`name` varchar(100) not null,
	`adress` varchar(500) default null,
	`phone`	 varchar(100) default null,
	`auth_user_id` int(11) not null,
        `date_create` TIMESTAMP default '0000-00-00 00:00:00',
        `date_updated` TIMESTAMP  default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8;

DROP TABLE IF EXISTS `profile_employees`;
CREATE TABLE IF NOT EXISTS `profile_employees`(
	`id` int(11) not null AUTO_INCREMENT,
	`name` varchar(100) not null,
	`profile_company_id` int(11) not null,
	`auth_user_id` int(11) not null,
        `date_create` TIMESTAMP default '0000-00-00 00:00:00',
        `date_updated` TIMESTAMP  default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8;

DROP TABLE IF EXISTS `profile_client_payments`;
CREATE TABLE IF NOT EXISTS `profile_client_payments`(
	`id` int(11) not null AUTO_INCREMENT,
	`client_id` int(11) not null,
	`payment_type_id` varchar(100) not null,
	`payment_data` varchar(100) not null,
        `date_create` TIMESTAMP default '0000-00-00 00:00:00',
        `date_updated` TIMESTAMP  default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8;

DROP TABLE IF EXISTS `profile_payment_types`;
CREATE TABLE IF NOT EXISTS `profile_payment_types`(
	`id` int(11) not null AUTO_INCREMENT,
	`name` varchar(100) not null,
        `date_create` TIMESTAMP default '0000-00-00 00:00:00',
        `date_updated` TIMESTAMP  default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8;

DROP TABLE IF EXISTS `project_projects`;
CREATE TABLE IF NOT EXISTS `project_projects`(
	`id` int(11) not null AUTO_INCREMENT,
	`name` varchar(100) not null,
	`company_id` varchar(500) not null,
        `date_create` TIMESTAMP default '0000-00-00 00:00:00',
        `date_updated` TIMESTAMP  default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8;

DROP TABLE IF EXISTS `project_advertisments`;
CREATE TABLE IF NOT EXISTS `project_advertisments`(
	`id` int(11) not null AUTO_INCREMENT,
	`name` varchar(100) not null,
	`project_id` varchar(500) not null,
        `date_create` TIMESTAMP default '0000-00-00 00:00:00',
        `date_updated` TIMESTAMP  default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8;


